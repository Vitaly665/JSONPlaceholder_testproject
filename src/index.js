import React from 'react'
import { render } from 'react-dom'
import App from './containers/App'
import './styles/app.css'
import './styles/bootstrap/bootstrap.css'


render(
    <div className='app'>
      <App />
    </div>,
  document.getElementById('root')
)