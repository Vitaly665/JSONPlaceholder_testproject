import React, { Component } from 'react'
import axios from 'axios'

export default class Comments extends Component {

    componentDidMount() {
        this.getComments(this.props.postIdApp);
    }

    componentWillReceiveProps(nextProps) {
        this.getComments(nextProps.postIdApp);
    }

    readmoreClick(e) {
        e.preventDefault();
        if ((this.state.visible) === true)
        {
            this.setState({ visible: false })
        }
        else
        {
            this.setState({ visible: true })
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            visible: false,
        }
        this.getComments = this.getComments.bind(this);
        this.readmoreClick = this.readmoreClick.bind(this);
    }

    getComments(postIdApp) {
        axios.get('https://jsonplaceholder.typicode.com/comments', {
            params: {
                postId: postIdApp
            }
        })
            .then(response =>
                this.setState({
                    comments: response.data
                }))
    }

    render() {
        var visible = this.state.visible;
        let templatePost = this.state.comments.map((p, index) =>
            <div className='comment' key={index}>
                <a className='comment__email' href='mailto:{p.email}'>{p.email}</a>
                <div className='comment__body'>{p.body}</div>
            </div>
        )
        return (
            <div>
                <div onClick={this.readmoreClick} className={'readMore'}>
                    {(visible ? 'Cкрыть комментарии' : 'Показать комментарии')}
                </div>
                <div className={(visible ? '' : 'none')}>
                    {templatePost}
                </div>
            </div>
        );
    }
}