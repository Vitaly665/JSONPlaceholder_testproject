import React, { Component } from 'react'
import axios from 'axios'
import Comments from './Comments.js'

export default class PostList extends Component {

  componentDidMount(){
    this.getPosts(this.props.userIdApp);
  }

  constructor(props) {
    super(props);
    this.state = {
      posts: [],
    }
    this.getPosts = this.getPosts.bind(this);
  }
  
  getPosts (userIdApp){
    axios.get('https://jsonplaceholder.typicode.com/posts', {
      params: {
        userId: userIdApp
      }
    })
      .then(response =>
        this.setState({
          posts: response.data
        }))
  }

  componentWillReceiveProps(nextProps) {
    this.getPosts(nextProps.userIdApp);
  }

  render() {
    let templatePost = this.state.posts.map((p, index) =>
    <div key={index}>
      <div className='post'>
        <div className='post__title' >{p.title}</div>
        <div className='post__body' >{p.body}</div>
      </div>
      <Comments postIdApp={p.id}/>
    </div>
      )
    return (
      <div>
        <h4>Список постов пользователя {this.props.userName}</h4>
        {templatePost}
      </div>

    );
  }
}