import React, { Component } from 'react'
import axios from 'axios'
import UserItem from './UserItem'

export default class UserList extends Component {

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/users/').then(response => {
      this.setState({
        users: response.data
      })
    })
  }

  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
  }

  moveUp(userId, name) {
    this.props.cbfunction(userId, name)
  }

  render() {
    const template = this.state.users.map((u, index) => {
      return (<UserItem key={index} name={u.name} userId={u.id} moveUp={this.moveUp.bind(this)} />)
    });
    return (
        <div className='userlist'>
          <h4>Список пользователей</h4>
          {template}
        </div>
          );
    }
  } 