import React, { Component } from 'react'

export default class UserItem extends Component {
  

  render() {
    return (
      <div>
        <button className='username' onClick={this.props.moveUp.bind(null, this.props.userId, this.props.name)}>
          {this.props.name}
        </button>
      </div>
    );
  }
}