import React, { Component } from 'react'
import UserList from '../components/UserList.js'
import PostList from '../components/PostList.js'

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userIdApp: 1, 
      userName: 'Leanne Graham',
    }
  }

  cbfunction(userId, name) {
    this.setState ({ userIdApp: userId, userName: name })
  }

  render() {
    return (
      <div className='main container'>
        <div className='row'>
          <div className='col-xs-3 col-sm-3 col-md-3 col-lg-3'>
            <UserList cbfunction={this.cbfunction.bind(this)} />
          </div>
          <div className='col-xs-8 col-sm-8 col-md-8 col-lg-8'>
            <PostList userIdApp={this.state.userIdApp} userName={this.state.userName}/>
          </div>
        </div>
      </div>
    )
  }
}